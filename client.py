"""The client sending commands and returning the response in JSON."""

import json
import socket
from cprint import *

import config as cfg


class Client:
    def __init__(self):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ADDR = (cfg.SERVER, cfg.PORT)
        self.client.connect(ADDR)
        print("[Connected]")
        self.connected = True
        self.idle()

    def idle(self):
        """Wait for a command."""
        cprint.info("[Type !help to get a list of available commands]")
        while self.connected:
            request = input("Type: ").upper()
            if request == "!DISCONNECT" or request == "!STOP":
                self.disconnect(request)
            else:
                self.send(request)
                self.receive()

    def send(self, msg):
        """Send a message to the server."""
        message = msg.encode(cfg.FORMAT)
        msg_length = len(message)
        send_length = str(msg_length).encode(cfg.FORMAT)
        send_length += b" " * (cfg.HEADER - len(send_length))
        self.client.send(send_length)
        self.client.send(message)

    def receive(self):
        """Receive a message from the server."""
        msg = self.client.recv(1024)
        received = json.loads(msg.decode(cfg.FORMAT))
        cprint.info(json.dumps(received, indent=4))

    def disconnect(self, msg):
        """Disconnect from the server, end script."""
        print("[Disconnected]")
        self.send(msg)
        self.connected = False


if __name__ == "__main__":
    client = Client()
