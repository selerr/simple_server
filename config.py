PORT = 5057
SERVER = "192.168.1.2"
FORMAT = "utf-8"
HEADER = 64
INFO = {"server_version": "2.0"}

COMMANDS = {
    "!DISCONNECT": "Disconnect client from server.",
    "!UPTIME": "Get time of server working in seconds.",
    "!INFO": "Get version of server, date of last modification and other informations.",
    "!HELP": "Get this list.",
    "!STOP": "Stop server and all connected clients.",
    "!LOGIN": "Log-in.",
    "!LOGOUT": "Log-out.",
    "!PRIV": "Send private message to another user.",
    "!MAILBOX": "Open mailbox, check messages or delete them.",
    "!ADD_USER": "Only for admins: add new user(s).",
    "!DELETE_USER": "Only for admis: delete user(s).",
}
